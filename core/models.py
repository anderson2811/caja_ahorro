from __future__ import unicode_literals
from material.frontend.apps import ModuleMixin

from django.db import models


class Persona(models.Model):
    name = models.CharField(max_length=200, verbose_name='Nombre')
    last_name = models.CharField(max_length=200, verbose_name='Apellido')
    cedula = models.IntegerField(verbose_name='Cedula')
    email = models.EmailField(verbose_name='Correo')

    class Meta:
        verbose_name = "Persona"
        verbose_name_plural = "Personas"

    def __unicode__(self):
        return '%s %s' % (self.name, self.last_name)


class Caja(models.Model):
    title = models.CharField(max_length=200, verbose_name='Titulo')
    monto_numero = models.FloatField(verbose_name='Monto por Numero')
    monto_multa = models.FloatField(verbose_name='Monto por Multa')
    personas = models.ManyToManyField(Persona, verbose_name='Socios',
                                      related_name='socios')
    admins = models.ManyToManyField(Persona, verbose_name='Adminitradores',
                                    related_name='admins')
    close_date = models.DateField(verbose_name='Fecha de Cierre')
    date = models.DateField(auto_now_add=True,
                            verbose_name='Fecha de registro')

    class Meta:
        verbose_name = "Caja"
        verbose_name_plural = "Cajas"

    def __unicode__(self):
        return '%s' % (self.title)


class Numero(models.Model):
    persona = models.ForeignKey(Persona, verbose_name='Socio',
                                related_name='Socio')
    caja = models.ForeignKey(Caja, verbose_name='Caja de ahorro',
                             related_name='Caja_ahorro')
    date = models.DateField(auto_now_add=True,
                            verbose_name='Fecha de registro')

    class Meta:
        verbose_name = "Numero"
        verbose_name_plural = "Numeros"

    def __unicode__(self):
        return '%s %s' % (self.persona, self.caja)


class Prestamo(models.Model):
    persona = models.ForeignKey(Persona, verbose_name='Socio',
                                related_name='Socio_prestamo')
    caja = models.ForeignKey(Caja, verbose_name='Caja de ahorro',
                             related_name='Caja_ahorro_prestamo')
    date = models.DateField(auto_now_add=True,
                            verbose_name='Fecha de registro')
    monto = models.FloatField(verbose_name='Monto')

    class Meta:
        verbose_name = "Prestamo"
        verbose_name_plural = "Prestamos"

    def __unicode__(self):
        return '%s %s' % (self.persona, self.caja)
