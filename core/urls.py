# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from views import Index, PersonasIndex

urlpatterns = [
    url(r'^$', login_required(Index.as_view()), name="index"),
    url(r'^persona/$', login_required(PersonasIndex.as_view()),
        name="index_persona"),
    ]
