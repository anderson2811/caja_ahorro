from __future__ import unicode_literals

from django.apps import AppConfig
from material.frontend.apps import ModuleMixin


class CoreConfig(ModuleMixin, AppConfig):
    name = 'core'
    verbose_name = 'Control'
    icon = '<i class="mdi-communication-quick-contacts-dialer"></i>'