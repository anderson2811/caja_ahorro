# -*- coding: utf-8 -*-
# from django.shortcuts import render
import json

from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.models import User
from models import Persona


class Login(TemplateView):
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(request.GET.get("next", False))
        return render_to_response(self.template_name, {},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        logout(request)
        next_ = request.GET['next'] if request.GET.get('next', False) else '/'
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        try:
            user = authenticate(
                    username=User.objects.get(email__iexact=email).username,
                    password=password)
        except:
            user = None
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(request.GET.get("next", False))
            else:
                resp = {'msj': "Contraseña valida, \
                                pero su Cuenta esta deshabilitada!.",
                        'error': True,
                        'url_redirect': False,
                        'icono': 'icon fa fa-warning',
                        'type': 'warning'}
        else:
            resp = {'msj': "Email o contraseña incorrectos.",
                    'error': True,
                    'url_redirect': False,
                    'icono': 'icon fa fa-ban',
                    'type': 'danger'}
        return render_to_response(self.template_name, {
                                        'resp': resp},
                                  RequestContext(request))


class Index(TemplateView):
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))


def lx_logout(request):
    try:
        logout(request)
    except:
        pass
    return HttpResponseRedirect('/')


class PersonasIndex(TemplateView):
    template_name = "persona/index.html"

    def get(self, request, *args, **kwargs):
        personas = Persona.objects.filter()
        return render_to_response(self.template_name, {
                                  'personas': personas
                                  },
                                  RequestContext(request))
