from django.contrib import admin
from models import Caja, Persona, Numero, Prestamo
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
# Register your models here.


def export_selected_objects(modeladmin, request, queryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    ct = ContentType.objects.get_for_model(queryset.model)
    return HttpResponseRedirect("/export/?ct=%s&ids=%s" % (ct.pk, ",".join(selected)))


class CajaAdmin(admin.ModelAdmin):
    list_filter = ('title', 'date')
    list_display = ('title', 'date')

    def save_model(self, request, obj, form, change):
        # custom stuff here
        obj.save()


class PersonaAdmin(admin.ModelAdmin):
    list_filter = ('name', 'last_name', 'cedula')
    list_display = ('name', 'last_name', 'cedula')

    def save_model(self, request, obj, form, change):
        # custom stuff here
        obj.save()


class NumeroAdmin(admin.ModelAdmin):
    list_filter = ('persona', 'caja', 'date')
    list_display = ('persona', 'caja', 'date')

    def save_model(self, request, obj, form, change):
        # custom stuff here
        obj.save()


class PrestamoAdmin(admin.ModelAdmin):
    list_filter = ('persona', 'caja', 'date')
    list_display = ('persona', 'caja', 'date')
    actions = ['export_selected_objects']

    def save_model(self, request, obj, form, change):
        # custom stuff here
        obj.save()


admin.site.register(Persona, PersonaAdmin)
admin.site.register(Caja, CajaAdmin)
admin.site.register(Numero, NumeroAdmin)
admin.site.register(Prestamo, PrestamoAdmin)
